package unisession

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGenerateRandomKey(t *testing.T) {
	key, err := generateRandomKey()
	fmt.Printf("key = %v\n", key)
	assert.Nil(t, err)
}
