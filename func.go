package unisession

import (
	"crypto/rand"
	"encoding/base64"
	"io"
	"net"
	"net/http"
	"strings"
)

const (
	XFowwardedFor = "X-Forwarded-For"
	XRealIp       = "X-Real-Ip"
	XClientIp     = "X-Client-Ip"
	XSessionId    = "X-Session-Id"
	XUserId       = "X-User-Id"
	XUid          = "X-Uid"
	XUserName     = "X-User-Name"
	XUserAvatar   = "X-User-Avatar"
	XTenantId     = "X-Tenant-Id"
	XDomainId     = "X-Domain-Id"
	XSubjectToken = "X-Subject-Token"
	XAuthToken    = "X-Auth-Token"
	XAccessToken  = "X-Access-Token"
	SESSION_ID    = "SessionId"
	SET_COOKIE    = "Set-Cookie"
)

// ClientIp 尽最大努力实现获取客户端 IP 的算法。
// 解析 X-Real-Ip 和 X-Forwarded-For 以便于反向代理（nginx 或 haproxy）可以正常工作。
func ClientIP(r *http.Request) string {
	forwardedFor := r.Header.Get(XFowwardedFor)
	ips := strings.Split(forwardedFor, ",")
	if len(ips) > 0 {
		ip := strings.TrimSpace(ips[0])
		if ip != "" {
			return ip
		}
	}

	realIp := r.Header.Get(XRealIp)
	ip := strings.TrimSpace(realIp)
	if ip != "" {
		return ip
	}

	if ip, _, err := net.SplitHostPort(strings.TrimSpace(r.RemoteAddr)); err == nil {
		return ip
	}
	return ""
}

// generateRandomKey returns a new random key
func generateRandomKey() (string, error) {
	k := make([]byte, 64)
	if _, err := io.ReadFull(rand.Reader, k); err != nil {
		return "", err
	}
	return base64.RawURLEncoding.EncodeToString(k), nil
}
