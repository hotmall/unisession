package unisession

import (
	"context"
	"errors"
	"net/http"
	"sync"
	"time"

	restful "github.com/emicklei/go-restful/v3"
	"github.com/redis/go-redis/v9"
)

const (
	keyPrefix = "/unisession/v1/sessions/"
)

var (
	instance *Session
	once     sync.Once

	errNoLogin = errors.New("user is not logged in")
)

type Session struct {
	client redis.UniversalClient
}

func Instance(client redis.UniversalClient) *Session {
	once.Do(func() {
		instance = &Session{
			client: client,
		}
	})
	return instance
}

func (s *Session) Login(clientIP string, expiration time.Duration) (sid string, err error) {
	if sid, err = generateRandomKey(); err != nil {
		return
	}
	key := keyPrefix + sid
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	if _, err = s.client.HSet(ctx, "X-Client-Ip", clientIP).Result(); err != nil {
		return
	}
	_, err = s.client.Expire(ctx, key, expiration).Result()
	return
}

func (s *Session) Logout(sid string) (err error) {
	key := keyPrefix + sid
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	_, err = s.client.Del(ctx, key).Result()
	return
}

func (s *Session) Get(sid string, field string) (value string, err error) {
	key := keyPrefix + sid
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	value, err = s.client.HGet(ctx, key, field).Result()
	return
}

func (s *Session) MGet(sid string, fields ...string) ([]interface{}, error) {
	key := keyPrefix + sid
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	return s.client.HMGet(ctx, key, fields...).Result()
}

func (s *Session) Set(sid string, field string, value interface{}) (val int64, err error) {
	key := keyPrefix + sid
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	val, err = s.client.HSet(ctx, key, field, value).Result()
	return
}

func (s *Session) MSet(sid string, values ...interface{}) (val bool, err error) {
	key := keyPrefix + sid
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	val, err = s.client.HMSet(ctx, key, values...).Result()
	return
}

func (s *Session) Expire(sid string, expiration time.Duration) (bool, error) {
	key := keyPrefix + sid
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	return s.client.Expire(ctx, key, expiration).Result()
}

func (s *Session) Filter(req *restful.Request, resp *restful.Response, chain *restful.FilterChain) {
	cookie, err := req.Request.Cookie(SESSION_ID)
	if err != nil {
		result := make(map[string]string, 1)
		result["error"] = errNoLogin.Error()
		resp.WriteHeaderAndEntity(http.StatusUnauthorized, result)
		return
	}
	sid := cookie.Value
	// clientIP := ClientIP(req.Request)

	// 遍历将 session 中的内容都拷贝到 header 中
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	key := keyPrefix + sid
	vals, err := s.client.HGetAll(ctx, key).Result()
	if err != nil {
		result := make(map[string]string, 1)
		result["error"] = err.Error()
		resp.WriteHeaderAndEntity(http.StatusUnauthorized, result)
		return
	}
	for k, v := range vals {
		// TODO: 还没找到有效的方法可以得到客户的真实 Ip，在生产环境，使用了全站加速，CDN 回源请求，Forwarded-For 中的 Ip 每次请求都会变化，导致客户需要不断重新登入的问题
		// 先将此处屏蔽掉，解决生产环境问题，将来安全加固时重新分析解决
		// if k == XClientIp && v != clientIP {
		// 	result := make(map[string]string, 1)
		// 	result["error"] = errNoLogin.Error()
		// 	resp.WriteHeaderAndEntity(http.StatusUnauthorized, result)
		// 	return
		// }
		req.Request.Header.Add(k, v)
	}
	req.Request.Header.Add(XSessionId, sid)
	chain.ProcessFilter(req, resp)
}

func (s *Session) NologinFilter(req *restful.Request, resp *restful.Response, chain *restful.FilterChain) {
	clientIP := ClientIP(req.Request)
	req.Request.Header.Add(XClientIp, clientIP)
	chain.ProcessFilter(req, resp)
}

func (s *Session) LogoutFilter(req *restful.Request, resp *restful.Response, chain *restful.FilterChain) {
	cookie, err := req.Request.Cookie(SESSION_ID)
	if err != nil {
		result := make(map[string]string, 1)
		result["error"] = errNoLogin.Error()
		resp.WriteHeaderAndEntity(http.StatusUnauthorized, result)
		return
	}
	sid := cookie.Value
	req.Request.Header.Add(XSessionId, sid)
	chain.ProcessFilter(req, resp)
}
